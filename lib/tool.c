
#include <bson/bson.h>

struct _bson_array_builder_t {
   uint32_t index;
   bson_t bson;
};

bson_t* bson_array_builder_copy(bson_array_builder_t* bab){
    BSON_ASSERT_PARAM (bab);
    bson_t* out = bson_copy(&bab->bson);
    return out;
}

bson_iter_t* bson_iter_new(){
    bson_iter_t* iter = malloc(sizeof(bson_iter_t));
    return iter;
}

// gcc -I. tool.c
// gcc -I. tool.c -fPIC -shared -o libtool.so

// gcc -o tool.o -c tool.c -I.
// ar -rsv libtool.a tool.o