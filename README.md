# bson4cj

https://gitcode.com/zswcode/bson4cj.git

#### 介绍

###### 使用仓颉调bson
###### 注意:仅ubuntu下跑过, 其他平台请先自行编译静态链接库

使用示例

```
    var kv1 = kvb("set","123")
    var valOpt = kv1.At<String>()

    var kvbMap = [
        kvb("bool", true),
        kvb("int32", Int32(123)),
        kvb("int64", 123),
        kvb("double", 1.2),
        kvb("str","xzy"),
        kvb("time",DateTime.now()),
        kvb("oid", ObjectId()),
        kvb("bson", Bson(kvb("key1", 1.2))),
        kvb("arr", BsonArray(true, false)),
        kvb("arr2", [1,2,3]),

        kvb("kvb", kvb("key2", 11)),
        kvb("val", value(true))
    ]
    
    var bson = Bson(kvbMap)
    var v1 = bson["arr"]

    var bobj = bson.parser()
    bobj.toString()

    var oid = ObjectId()

    var oid2 = ObjectId("66b34ca7ce96fb0291023181")

    var date = oid2.Timestamp()

    var req = Bson()
    req["bool"] = true
    req["int32"] = Int32(123)
    req["int64"] = 321
    req["double"] = 1.2
    req["str"] = "abc"
    req["time"] = DateTime.now()
    req["oid"] = oid
    req["bson"] = Bson(kvb("key1",1))
    req["arr"] = BsonArray(1,2,3)
    req["arr2"] = [3,2,1]

    req["kvb"] = kvb("a1", 1)
    req["value"] = value(1)

    var arr = BsonArray(-1,-1,-1)
    arr.append(true)
    arr.append(Int64(0))
    arr.append(1.2)
    arr.append("abc")
    arr.append(DateTime.now())
    arr.append(ObjectId())
    arr.append(Bson(kvb("a1",2)))

    arr.append(kvb("a1",2))
    arr.append(value(123))

```